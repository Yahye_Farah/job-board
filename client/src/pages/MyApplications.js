import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../components/Spinner";
import ApplicationItem from "../features/applications/ApplicationItem";
import { getUserApplications } from "../features/users/usersService";

export default function MyApplications() {
  const dispatch = useDispatch();
  const { user, token } = useSelector((state) => state.auth);
  const { applications, isLoading, isSuccess } = useSelector(
    (state) => state.users
  );

  useEffect(() => {
    dispatch(getUserApplications({ token, id: user.id }));
  }, []);

  if (isLoading) return <Spinner />;
  if (applications.length === 0)
    return (
      <div>
        <h2>You have no applications yet</h2>
      </div>
    );
  return (
    <>
      {applications.map((application) => (
        <ApplicationItem application={application} showButton={false} />
      ))}
    </>
  );
}

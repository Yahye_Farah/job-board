import JobListing from "../features/jobs/JobListing";

export default function Jobs() {
  return (
    <div className="container mx-auto my-2">
      <JobListing />
    </div>
  );
}

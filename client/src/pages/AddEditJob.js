import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Spinner from "../components/Spinner";
import { createJob, getJobById, updateJob } from "../features/job/jobService";
import { reset } from "../features/job/jobSlice";

const editData = {
  title: "",
  description: "",
};

export default function AddEditJob({ type }) {
  const dispatch = useDispatch();
  const { id } = useParams();
  const navigate = useNavigate();
  const onEditDataChange = (e) =>
    setEdit({ ...edit, [e.target.name]: e.target.value });
  const { job, isLoading, isError, isSuccess, message, jobReceived } =
    useSelector((state) => state.job);
  const [edit, setEdit] = useState(editData);
  const [jobRequested, setJobRequested] = useState(false);

  const token = useSelector((state) => state.auth.token);

  useEffect(() => {
    if (isLoading) return;
    if (isSuccess) {
      navigate("/");
    }
    if (type === "EDIT" && !jobRequested) {
      setJobRequested(true);
      dispatch(getJobById({ id, token }));
    }
    if (jobReceived) {
      setEdit({ title: job?.title || "", description: job?.description || "" });
    }
    return () => dispatch(reset());
  }, [isSuccess, jobReceived]);

  const onSubmit = (e) => {
    e.preventDefault();

    if (edit.title && edit.description) {
      if (type === "EDIT") dispatch(updateJob({ data: edit, id, token }));
      if (type === "ADD") dispatch(createJob({ data: edit, id, token }));
    }
  };

  if (isLoading) {
    return <Spinner />;
  }
  console.log("state", edit);
  return (
    <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <form className="mt-8 space-y-6">
          <div className="rounded-md shadow-sm -space-y-px">
            <div className="mb-3">
              <label htmlFor="title" className="sr-only">
                Title
              </label>
              <input
                value={edit.title}
                onChange={onEditDataChange}
                id="title"
                name="title"
                type="title"
                required
                className="appearance-none rounded-none relative block w-full  px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="title"
              />
            </div>
            <div>
              <label htmlFor="description" className="sr-only">
                Description
              </label>
              <textarea
                value={edit.description}
                rows={4}
                onChange={onEditDataChange}
                id="description"
                name="description"
                type="description"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="description"
              />
            </div>
          </div>
          <div>
            <button
              onClick={onSubmit}
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                <svg
                  className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Spinner from "../components/Spinner";
import {
  getApplicationById,
  updateApplication,
} from "../features/application/applicationService";
import ApplicationItem from "../features/applications/ApplicationItem";

export default function ApplicationView() {
  const { id, app_id } = useParams();
  const { token, user } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const { isLoading, isSuccess, application } = useSelector(
    (state) => state.application
  );

  useEffect(() => {
    dispatch(getApplicationById({ id, app_id, token }));
    if (user.is_admin) {
      dispatch(
        updateApplication({ token, id, app_id, data: { status: true } })
      );
    }
  }, []);

  if (isLoading || !application) return <Spinner />;
  return (
    <>
      <ApplicationItem id={id} application={application} />
    </>
  );
}

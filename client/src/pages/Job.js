import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import ApplyJobForm from "../components/ApplyJobForm";
import Spinner from "../components/Spinner";
import ApplicationsListing from "../features/applications/ApplicationsListing";
import { getJobById } from "../features/job/jobService";
import { reset } from "../features/job/jobSlice";
import JobListingItem from "../features/jobs/JobListingItem";

export default function Job() {
  const dispatch = useDispatch();
  const { isLoading, job } = useSelector((state) => state.job);
  const user = useSelector((state) => state.auth.user);
  const token = useSelector((state) => state.auth.token);
  const { id } = useParams();

  useEffect(() => {
    dispatch(getJobById({ id, token }));
    return () => dispatch(reset());
  }, []);

  if (isLoading || !job) return <Spinner />;
  return (
    <div className="container mx-auto my-2">
      <JobListingItem job={job} />
      {!user.is_admin && <ApplyJobForm job={job} />}
      {user.is_admin && <ApplicationsListing job={job} id={id} />}
    </div>
  );
}

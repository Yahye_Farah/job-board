import { useSelector } from "react-redux";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

export default function ProtectedRoutes({ children }) {
  const user = useSelector((state) => state.auth);
  console.log("useruser", user);
  if (!user.user) return <Navigate to="/login" replace />;

  return children;
}

import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../features/auth/authSlice";
import jobsReducer from "../features/jobs/jobsSlice";
import usersReducer from "../features/users/usersSlice";
import jobReducer from "../features/job/jobSlice";
import applicationsReducer from "../features/applications/applicationsSlice";
import applicationReducer from "../features/application/applicationSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    jobs: jobsReducer,
    users: usersReducer,
    job: jobReducer,
    applications: applicationsReducer,
    application: applicationReducer,
  },
});

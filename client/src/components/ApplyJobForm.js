import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createApplication } from "../features/application/applicationService";
import { reset } from "../features/application/applicationSlice";
import Spinner from "./Spinner";

export default function ApplyJobForm({ job }) {
  const [cover, setCover] = useState({ cover_letter: "" });
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const token = useSelector((state) => state.auth.token);
  const { isLoading, isSuccess } = useSelector((state) => state.application);
  const onCoverLetterChange = (e) => {
    setCover({ cover_letter: e.target.value });
  };
  const onSubmit = (e) => {
    e.preventDefault();
    if (cover) {
      dispatch(createApplication({ token, id: job.id, data: cover }));
    }
  };
  useEffect(() => {
    if (isSuccess) {
      navigate("/");
    }
    return () => dispatch(reset());
  }, [isSuccess]);
  if (isLoading) return <Spinner />;
  return (
    <form className="mt-8 space-y-6">
      <div className="rounded-md shadow-sm -space-y-px">
        <div>
          <label htmlFor="email-address" className="sr-only">
            Cover Letter
          </label>
          <textarea
            rows={6}
            onChange={onCoverLetterChange}
            id="cover-letter"
            name="cover_letter"
            required
            className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
            placeholder="cover letter"
          />
        </div>
        <div>
          <button
            onClick={onSubmit}
            type="submit"
            className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            <span className="absolute left-0 inset-y-0 flex items-center pl-3">
              <svg
                className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                  clipRule="evenodd"
                />
              </svg>
            </span>
            Apply
          </button>
        </div>
      </div>
    </form>
  );
}

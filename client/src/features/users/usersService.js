import { createAsyncThunk } from "@reduxjs/toolkit";
import API from "../../app/api";
import { getAuthHeader } from "../../utils/getAuthHeader";

export const getUsers = createAsyncThunk(
  "users/getAll",
  async (_, thunkAPI) => {
    try {
      const response = await API.get("users");
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const getUserApplications = createAsyncThunk(
  "users/getApplications",
  async ({ id, token }, thunkAPI) => {
    try {
      const response = await API.get(
        `users/${id}/applications`,
        getAuthHeader(token)
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const getUserApplication = createAsyncThunk(
  "users/getApplication",
  async (data, thunkAPI) => {
    try {
      const response = await API.get(
        `users/${data.user_id}/applications/${data.application_id}`
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const createUserApplication = createAsyncThunk(
  "users/getApplication",
  async ({ data, user_id, application_id }, thunkAPI) => {
    try {
      const response = await API.post(
        `users/${user_id}/applications/${application_id}`,
        data
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const updateUserApplication = createAsyncThunk(
  "users/updateApplication",
  async ({ user_id, application_id, data }, thunkAPI) => {
    try {
      const response = await API.put(
        `users/${user_id}/applications/${application_id}`,
        data
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const deleteUserApplication = createAsyncThunk(
  "users/deleteApplications",
  async (data, thunkAPI) => {
    try {
      const response = await API.delete(
        `users/${data.user_id}/applications/${data.application_id}`
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

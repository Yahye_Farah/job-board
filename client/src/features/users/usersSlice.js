import { createSlice } from "@reduxjs/toolkit";
import {
  createUserApplication,
  deleteUserApplication,
  getUserApplications,
  getUsers,
  updateUserApplication,
} from "./usersService";

const initialState = {
  applications: [],
  users: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};

export const usersSlice = createSlice({
  name: "applications",
  initialState,
  reducers: {
    reset: (state) => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(getUsers.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getUsers.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.users = action.payload;
      })
      .addCase(getUsers.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })

      .addCase(getUserApplications.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getUserApplications.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.applications = action.payload;
      })
      .addCase(getUserApplications.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })

      .addCase(createUserApplication.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateUserApplication.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.jobs = state.jobs.filter((app) => app._id !== action.payload.id);
        state.jobs.push(action.payload);
      })
      .addCase(updateUserApplication.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(updateUserApplication.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createUserApplication.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.applications.push(action.payload);
      })
      .addCase(createUserApplication.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(deleteUserApplication.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteUserApplication.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.applications = state.applications.filter(
          (appl) => appl._id !== action.payload.id
        );
      })
      .addCase(deleteUserApplication.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});

export const { reset } = usersSlice.actions;
export default usersSlice.reducer;

import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../../components/Spinner";
import JobListingItem from "./JobListingItem";
import { getJobs } from "./jobsService";
import { reset } from "./jobsSlice";

export default function JobListing() {
  const dispatch = useDispatch();
  const { isLoading, jobs } = useSelector((state) => state.jobs);
  const token = useSelector((state) => state.auth.token);
  useEffect(() => {
    dispatch(getJobs({ token }));
    return () => dispatch(reset());
  }, []);
  if (isLoading) return <Spinner />;
  if (jobs.length === 0) {
    return (
      <div>
        <h2>No Jobs has been posted yet</h2>
      </div>
    );
  }
  return (
    <>
      {jobs.map((job) => (
        <JobListingItem job={job} key={job.id} />
      ))}
    </>
  );
}

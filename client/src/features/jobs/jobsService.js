import { createAsyncThunk } from "@reduxjs/toolkit";
import API from "../../app/api";
import { getAuthHeader } from "../../utils/getAuthHeader";

export const getJobs = createAsyncThunk(
  "jobs/getAll",
  async ({ token }, thunkAPI) => {
    try {
      const response = await API.get("jobs", getAuthHeader(token));
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const deleteJob = createAsyncThunk(
  "jobs/delete",
  async ({ id, token }, thunkAPI) => {
    try {
      const response = await API.delete(`jobs/${id}`, getAuthHeader(token));
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

//ADMIN ONLY

export const getJobApplications = createAsyncThunk(
  "jobs/applications",
  async (id, thunkAPI) => {
    try {
      const response = await API.get(`jobs/${id}/applications`);
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const getJobApplication = createAsyncThunk(
  "jobs/application",
  async (data, thunkAPI) => {
    try {
      const response = await API.get(
        `jobs/${data.id}/applications/${data.application_id}`
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

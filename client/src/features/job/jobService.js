import { createAsyncThunk } from "@reduxjs/toolkit";
import API from "../../app/api";
import { getAuthHeader } from "../../utils/getAuthHeader";

export const getJobById = createAsyncThunk(
  "jobs/getAllById",
  async ({ id, token }, thunkAPI) => {
    console.log("id and token", id, token);
    try {
      const response = await API.get(`jobs/${id}`, getAuthHeader(token));
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const createJob = createAsyncThunk(
  "jobs/create",
  async ({ data, token }, thunkAPI) => {
    try {
      const response = await API.post("jobs/", data, getAuthHeader(token));
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const updateJob = createAsyncThunk(
  "jobs/update",
  async ({ data, id, token }, thunkAPI) => {
    try {
      const response = await API.put(`jobs/${id}`, data, getAuthHeader(token));
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

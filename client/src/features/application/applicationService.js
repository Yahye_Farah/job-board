import { createAsyncThunk } from "@reduxjs/toolkit";
import API from "../../app/api";
import { getAuthHeader } from "../../utils/getAuthHeader";

export const getApplicationById = createAsyncThunk(
  "jobs/getApplicationById",
  async ({ id, token, app_id }, thunkAPI) => {
    try {
      const response = await API.get(
        `jobs/${id}/applications/${app_id}`,
        getAuthHeader(token)
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const createApplication = createAsyncThunk(
  "application/create",
  async ({ data, id, token }, thunkAPI) => {
    try {
      const response = await API.post(
        `jobs/${id}/applications`,
        data,
        getAuthHeader(token)
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const updateApplication = createAsyncThunk(
  "application/update",
  async ({ data, id, token, app_id }, thunkAPI) => {
    try {
      const response = await API.put(
        `jobs/${id}/applications/${app_id}`,
        data,
        getAuthHeader(token)
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../../components/Spinner";
import ApplicationItem from "./ApplicationItem";
import { getApplications } from "./applicationsService";

export default function ApplicationsListing({ job, id }) {
  const dispatch = useDispatch();
  const { isLoading, isSuccess, applications } = useSelector(
    (state) => state.applications
  );
  const token = useSelector((state) => state.auth.token);
  useEffect(() => {
    dispatch(getApplications({ id, token }));
  }, []);

  if (isLoading) return <Spinner />;
  return (
    <>
      <div>Application Listing</div>
      {applications.map((application) => (
        <ApplicationItem application={application} id={id} />
      ))}
    </>
  );
}

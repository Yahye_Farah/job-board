import { createSlice } from "@reduxjs/toolkit";
import { getApplications } from "./applicationsService";

const initialState = {
  applications: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};

export const applicationsSlice = createSlice({
  name: "applications",
  initialState,
  reducers: {
    reset: (state) => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(getApplications.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getApplications.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.applications = action.payload;
      })
      .addCase(getApplications.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});

export const { reset } = applicationsSlice.actions;
export default applicationsSlice.reducer;

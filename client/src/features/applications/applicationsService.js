import { createAsyncThunk } from "@reduxjs/toolkit";
import API from "../../app/api";
import { getAuthHeader } from "../../utils/getAuthHeader";

export const getApplications = createAsyncThunk(
  "applications/getAll",
  async ({ id, token }, thunkAPI) => {
    try {
      const response = await API.get(
        `jobs/${id}/applications`,
        getAuthHeader(token)
      );
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

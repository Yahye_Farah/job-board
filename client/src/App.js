import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Jobs from "./pages/Jobs";
import Job from "./pages/Job";
import ProtectedRoutes from "./pages/ProtectedRoutes";
import AddEditJob from "./pages/AddEditJob";
import ApplicationView from "./pages/ApplicationView";
import MyApplications from "./pages/MyApplications";

function App() {
  return (
    <div className="h-full w-full">
      <Router>
        <div>
          <Header />
          <Routes>
            <Route
              path="/"
              element={
                <ProtectedRoutes>
                  <Jobs />
                </ProtectedRoutes>
              }
            />
            <Route
              path="/jobs/:id"
              element={
                <ProtectedRoutes>
                  <Job />
                </ProtectedRoutes>
              }
            />
            <Route
              path="/jobs/editJob/:id"
              element={
                <ProtectedRoutes>
                  <AddEditJob type="EDIT" />
                </ProtectedRoutes>
              }
            />
            <Route
              path="/jobs/:id/applications/:app_id"
              element={
                <ProtectedRoutes>
                  <ApplicationView />
                </ProtectedRoutes>
              }
            />

            <Route
              path="/myapplications"
              element={
                <ProtectedRoutes>
                  <MyApplications />
                </ProtectedRoutes>
              }
            />

            <Route
              path="/jobs/addJob"
              element={
                <ProtectedRoutes>
                  <AddEditJob type="ADD" />
                </ProtectedRoutes>
              }
            />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;

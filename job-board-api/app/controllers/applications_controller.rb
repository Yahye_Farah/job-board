class ApplicationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_job
  before_action :set_application, only: [:show, :update, :destroy]
  before_action :set_applications, only: [:index]

  def index
    json_response(@applications)
  end

  def show
    json_response(@application)
  end

  def create 
    previousApplication = @current_user.applications.select {|application| application.job_id == params[:job_id].to_i}
    if previousApplication.length > 0
      raise(ExceptionHandler::MissingToken, Message.already_applied)
    end
    application = @current_user.applications.create!(application_params) 
    json_response(application, :created)
  end

  def update
    @application.update(application_params)
    head :no_content
  end

  
  def destroy
    @application.destroy
    head :no_content
  end

  private

  def application_params
    params.permit(:status, :cover_letter, :job_id)
  end

  def set_job 
    if params[:job_id]
      @job = Job.find(params[:job_id])
    else
      @user = User.find(params[:user_id])
      puts @user.inspect
    end
  end

  def set_application
    if params[:job_id]
      @application = @job.applications.find_by!(id: params[:id]) 
    else
      @application = @user.applications.find_by(id: params[:id])
    end
  end

  def set_applications
    if params[:job_id]
      @applications = @job.applications 
    else
      @applications = @user.applications 
    end
  end
end

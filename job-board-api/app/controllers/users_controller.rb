class UsersController < ApplicationController
  load_and_authorize_resource :except => [:create]
  skip_before_action :authorize_request, only: :create
  before_action :set_user, only: [:show, :update, :destroy]

  def create
    user = User.create!(user_params)
    data = AuthenticateUser.new(user.email, user.password).call
    response = {message: Message.account_created, data:  data,}
    json_response(response, :created)
  end


  def index
    @users = User.all()
    json_response(@users)
  end

  def show
    json_response(@user)
  end

  def update
    @user.update(job_params)
  end

  def destroy
    @user.destroy
    head :no_content
  end

  private

  def set_user
    @user = User.find(params[:id])
  end 
  def user_params
    params.permit(
      :name,
      :email,
      :password,
      :password_confirmation 
    )
  end

end

class JobsController < ApplicationController
  load_and_authorize_resource
  before_action :set_job, only: [:show, :update, :destroy]

  def index
    @jobs = Job.all()
    json_response(@jobs)
  end

  def show
    json_response(@job)
  end

  def create
    puts current_user.inspect
    @job = current_user.jobs.create!(job_params)
    json_response(@job, :created)
  end

  def update
    @job.update(job_params)
  end

  def destroy
    @job.destroy
    head :no_content
  end

  private

  def job_params
    params.permit(:title, :description)
  end

  def set_job
    @job = Job.find(params[:id])
  end 

end

class Job < ApplicationRecord
  belongs_to :user
  
  has_many :applications, dependent: :nullify

  validates_presence_of :title, :description
end

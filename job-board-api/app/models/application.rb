class Application < ApplicationRecord
  belongs_to :job
  belongs_to :user

  validates_presence_of :cover_letter
end

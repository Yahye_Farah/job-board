require 'rails_helper'

RSpec.describe Application, type: :model do
  it { should validate_presence_of(:cover_letter) }
  it { should belong_to(:user) }
  it { should belong_to(:job) }
end

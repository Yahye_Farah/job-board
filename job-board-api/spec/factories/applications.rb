FactoryBot.define do
  factory :application do
    cover_letter { Faker::Lorem.word }
  end
end
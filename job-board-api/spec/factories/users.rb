# spec/factories/users.rb
FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    email {Faker::Internet.email}
    password 'balabala'
    is_admin false
  end
end
FactoryBot.define do
  factory :job do
    title { Faker::Lorem.word }
    description { Faker::Lorem.word }
  end
end
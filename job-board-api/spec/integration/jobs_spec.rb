# spec/integration/jobs_spec.rb
require 'swagger_helper'

describe 'Get all Jobs' do

  path '/jobs' do

    get 'get all jobs' do
      tags 'Jobs'
      security [bearerAuth: []]
      consumes 'application/json', 'application/xml' 
      response '200', 'success' do
        schema type: 'array', items: { '$ref' => '#/definitions/job' }
      end

      response '422', 'invalid request' do
        let(:'Accept') { 'application/foo' }
        # run_test!
      end
    end
  end

  path '/jobs' do
    post 'Creates a job' do
      tags 'Jobs'
      security [bearerAuth: []]

      consumes 'application/json', 'application/xml'
      parameter name: :job, in: :body, schema: {
        type: :object,
        properties: {
          title: { type: :string },
          description: { type: :string },
        },
        required: [ 'title', 'description' ]
      }

      response '201', 'Job created' do
        let(:job) { { name: 'Backend', description: 'Hello there' } }
        # run_test!
      end

      response '422', 'invalid request' do
        let(:name) { { name: 'foo' } }
        # run_test!
      end
    end
  end


  path '/jobs/{id}' do
    put 'Updates a job' do
      tags 'Jobs'
      security [bearerAuth: []]

      consumes 'application/json', 'application/xml'
      parameter name: :id, in: :path, :type => :string
      parameter name: :job, in: :body, schema: {
        type: :object,
        properties: {
          title: { type: :string },
          description: { type: :string },
        },
        required: [ 'id','title', 'description' ]
      }

      response '204', 'Job updated' do
        let(:job) { { name: 'Backend', description: 'Hello there' } }
        # run_test!
      end

      response '422', 'invalid request' do
        let(:name) { { name: 'foo' } }
        # run_test!
      end
    end
  end


  path '/jobs/{id}' do

    get 'Retrieves a job' do
      tags 'Jobs'
      security [bearerAuth: []]
      consumes 'application/json', 'application/xml' 
      parameter name: :id, :in => :path, :type => :string
      response '200', 'name found' do
        schema type: 'object', 
        properties: { 
          id: {type: :integer,},
          title: {type: :string},
          description: {type: :string}
        },
        required: ['id', 'name']   
      end

      response '404', 'job not found' do
        let(:id) { 'invalid' }
        # run_test!
      end
    end
  end


  path '/jobs/{id}' do

    delete 'Deletes a job' do
      tags 'Jobs'
      security [bearerAuth: []]
      consumes 'application/json', 'application/xml' 
      parameter name: :id, :in => :path, :type => :string
      response '200', 'name found' do
        schema type: 'object', 
        properties: { 
          id: {type: :integer,},
          title: {type: :string},
          description: {type: :string}
        },
        required: ['id', 'name']   
      end

      response '404', 'job not found' do
        let(:id) { 'invalid' }
        # run_test!
      end
    end
  end

  path '/jobs/{id}/applications' do

    get 'Retrieves a job applications' do
      tags 'Jobs'
      security [bearerAuth: []]
      consumes 'application/json', 'application/xml' 
      parameter name: :id, :in => :path, :type => :string
      response '200', 'name found' do
        schema type: 'array', items: { '$ref' => '#/definitions/application' },
        required: ['id', 'name', 'status']   
      end

      response '404', 'job not found' do
        let(:id) { 'invalid' }
        # run_test!
      end
    end
  end

  path '/jobs/{id}/applications/{app_id}' do

    get 'Retrieves a job application by id' do
      tags 'Jobs'
      security [bearerAuth: []]
      consumes 'application/json', 'application/xml' 
      parameter name: :id, :in => :path, :type => :string
      parameter name: :app_id, :in => :path, :type => :string
      response '200', 'name found' do
        schema type: 'object', 
        properties: { 
          id: {type: :integer,},
          app_id: {type: :integer},
          cover_letter: {type: :string}
        },
        required: ['id', 'name', 'app_id']   
      end

      response '404', 'job not found' do
        let(:id) { 'invalid' }
        # run_test!
      end
    end
  end

end
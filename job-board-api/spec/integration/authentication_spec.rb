# spec/integration/authentication_spec.rb
require 'swagger_helper'

describe 'Authentication API' do

  path '/auth/signup' do

    post 'Signups a new user' do
      tags 'Auth'
      consumes 'application/json', 'application/xml'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          email: { type: :string },
          password: { type: :string },
          password_confirmation: { type: :string }
        },
        required: [ 'name', 'email', 'password', 'password_confirmation' ]
      }

      response '201', 'User created' do
        let(:user) { { name: 'Dodo', email: 'tadha@tadha.com', password:"123", password_confirmation:'123'  } }
        # run_test!
      end

      response '422', 'invalid request' do
        let(:name) { { name: 'foo' } }
        # run_test!
      end
    end
  end


  path '/auth/login' do

    post 'Sign in  a user' do
      tags 'Auth'
      consumes 'application/json', 'application/xml'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string },
          password: { type: :string },
        },
        required: [ 'name', 'email' ]
      }

      response '200', 'User logs in' do
        let(:user) { { name: 'bala@bala.com', email: 'bala@bala.com' } }
        # run_test!
      end

      response '404', 'user not found' do
        let(:name) { 'invalid' }
        # run_test!
      end
    end
  end

end
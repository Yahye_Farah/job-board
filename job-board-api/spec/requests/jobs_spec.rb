require 'rails_helper'

RSpec.describe 'Jobs API', type: :request do
  let(:user) { create(:user) }
  let!(:jobs) { create_list(:job, 10, user_id: user.id) }
  let(:job_id) { jobs.first.id }
  # authorize request
  let(:headers) { valid_headers }

  describe 'GET /jobs' do
    before { get '/jobs', params: {}, headers: headers }

    it 'returns jobs' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /jobs/:id' do
    before { get "/jobs/#{job_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the job' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(job_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:job_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Job/)
      end
    end
  end

   describe 'POST /jobs' do
    
    # valid payload
    let(:valid_attributes) { { title: 'Backend', description: 'Backend developer are needed' } }
    context 'when the request is valid' do
      before { post '/jobs', params: valid_attributes.to_json, headers: headers }

      it 'user should not creates a job' do
        expect(json['title']).to eq(nil)
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'POST /jobs' do
    let(:user) {create(:user, is_admin:true)}
    let(:headers) { valid_headers }

    let(:valid_attributes) { { title: 'Backend', description: 'Backend developer are needed' } }
    context 'when the request is valid' do
      before { post '/jobs', params: valid_attributes.to_json, headers: headers }

      it 'admin creates a job' do
        expect(json['title']).to eq('Backend')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/jobs', params: { title: 'Foobar' }.to_json, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Description can't be blank/)
      end
    end
  end

  describe 'PUT /jobs/:id' do
    let(:valid_attributes) { { title: 'Frontend' } }

    context 'when the record exists' do
      before { put "/jobs/#{job_id}", params: valid_attributes.to_json, headers: headers }

      it 'should not update the record' do
        expect(response.body).to  match(/You are not authorized to access this page/)
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'PUT /jobs/:id' do
   let(:user) {create(:user, is_admin:true)}
   let(:headers) { valid_headers }
   let(:valid_attributes) { { title: 'Frontend' } }

   context 'when the record exists' do
     before { put "/jobs/#{job_id}", params: valid_attributes.to_json, headers: headers }

     it 'should update the record' do
       expect(response.body).to  be_empty
     end

     it 'returns status code 204' do
       expect(response).to have_http_status(204)
     end
   end
 end

  describe 'DELETE /jobs/:id' do
    before { delete "/jobs/#{job_id}", params: {}, headers: headers }

    it 'user should not delete a job code 401' do
      expect(response).to have_http_status(401)
    end
  end

  describe 'DELETE /jobs/:id' do
    let(:user) {create(:user, is_admin:true)}
    let(:headers) { valid_headers }
    before { delete "/jobs/#{job_id}", params: {}, headers: headers }

    it 'Admin can delete job' do
      expect(response).to have_http_status(204)
    end
  end

end
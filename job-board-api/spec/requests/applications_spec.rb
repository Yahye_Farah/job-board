require 'rails_helper'

RSpec.describe 'Applications API', type: :request do
  let(:user) { create(:user) }
  let!(:jobs) { create_list(:job, 10, user_id: user.id) }
  let(:job_id) { jobs.first.id }
  let!(:applications) { create_list(:application, 10, user_id: user.id, job_id: job_id) }
  let(:application_id) { applications.first.id }
  
 
  # authorize request
  let(:headers) { valid_headers }

  describe 'GET /jobs/:id/applications' do
    before { get "/jobs/#{job_id}/applications", params: {}, headers: headers }

    it 'returns job applications' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /jobs/:id/applications/application_id' do
    before { get "/jobs/#{job_id}/applications/#{application_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the specified application' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(job_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:application_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Application /)
      end
    end
  end

  describe 'POST /jobs/job_id/applications' do
    # valid payload
    let(:valid_attributes) { { cover_letter: 'Backend' } }
    context 'when the request is valid' do
      before { post "/jobs/#{jobs.second.id}/applications", params: valid_attributes.to_json, headers: headers }
      
      it 'creates a application' do
        expect(json['cover_letter']).to eq('Backend')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post "/jobs/#{jobs.third.id}/applications", params: { name: 'Gele' }.to_json, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Cover letter can't be blank/)
      end
    end
  end

  describe 'PUT /jobs/:job_id/applications/:application_id' do
   let(:valid_attributes) { { cover_letter: 'Frontend' } }

   context 'when the record exists' do
     before { put "/jobs/#{job_id}/applications/#{application_id}", params: valid_attributes.to_json, headers: headers }

     it 'should update the record' do
      expect(response.body).to  be_empty
    end

     it 'returns status code 204' do
       expect(response).to have_http_status(204)
     end
   end
 end

  describe 'DELETE /jobs/:job_id/applications/application_id' do
    before { delete "/jobs/#{job_id}/applications/#{application_id}", params: {}, headers: headers }

    it 'user should delete application code 204' do
      expect(response).to have_http_status(204)
    end
  end

  describe 'DELETE /jobs/:id' do
    let(:user) {create(:user, is_admin:true)}
    let(:headers) { valid_headers }
    before { delete "/jobs/#{job_id}/applications/#{application_id}", params: {}, headers: headers }

    it 'Admin should not  delete job' do
      expect(response).to have_http_status(401)
    end
  end

end
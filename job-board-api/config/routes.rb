Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  resources :jobs do
    resources :applications
  end

  resources :users do
    resources :applications
  end

  post 'auth/signup', to: 'users#create'
  post 'auth/login', to: 'authentication#authenticate'
end

# JOB-BOARD

## Development

### API

    cd job-board-api
    bundle install
    rails db:migrate
    rails s

### CLIENT

      cd client
      npm i
      npm start

### If you prefer docker

    docker build -t myimage .
    docker run -d -p 3000:3000 myimage

    or just

    docker-compose build
    docker-compose up

## for tests

    docker run myimage run_tests.sh

## some of the API endpoints are available on Swagger

    http://localhost:<PORT>/api-docs/index.html
